public class Person {
    private String name;
    private String vorname;

    Person() {

    }

    public void setName(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public void setVorname(String vorname){
        this.vorname = vorname;
    }

    public String getVorname(){
        return vorname;
    }
}
