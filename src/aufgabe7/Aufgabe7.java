package aufgabe7;

public class Aufgabe7 {
	
	static void strecke(Dialog dialog, int x1, int y1, int x2, int y2) {
		if( (-1 <= (x1-x2) &&  (x1-x2) <= 1) && (-1 <= (y1-y2) &&  (y1-y2)<= 1) ){
			dialog.markiere(x1,y1);
			dialog.markiere(x2,y2);
		}
		else {
			int mx = (x1 + x2)/2;
			int my = (y1 + y2)/2;
			strecke(dialog,x1,y1,mx,my);
			strecke(dialog,mx+1, my+1, x2, y2);
		}
	}

	public static void main(String[] args) {
		Dialog dialog = new Dialog();
				
		// Zeichne eine Linie von (10,10) nach (100,100)
		// Nicht rekursiv, sondern einfach Punkt f�r Punkt zeichnen
		for (int i = 10; i < 100; i++) {
			//dialog.markiere(i,i);
		}
		
		// Zeichne eine Linie von (10,100) nach (100,10)
		// Rekursive L�sung
		strecke(dialog, 10, 10, 100, 100);
		
	}

}
