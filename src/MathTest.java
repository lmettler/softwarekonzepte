import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

public class MathTest {

        @Test
        public void testMax() {
            assertEquals(10, Math.max(10, 3));
            assertEquals(200, Math.max(35, 200));
        }
}
