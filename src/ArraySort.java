import java.util.Arrays;
import java.util.Random;

public class ArraySort {
        public static void main(String[] args) {
            final int size = 200_000_000;
            Random random = new Random();
            int[] bigArray = new int[size];
            for (int i=0; i<bigArray.length;i++) {
                bigArray[i] = random.nextInt();
            }
            long start = System.currentTimeMillis();
            Arrays.sort(bigArray);
            long stop = System.currentTimeMillis();
            System.out.println("Time: " + (stop-start) + " ms");
        }
}
