public class complexity{

    static int sum;
    static int n = 64000;

    public complexity(){

    }

    public void aufg1() { //n
        sum = 0;
        long start = System.currentTimeMillis();
        for (int i=0; i<n; i++) {
            sum++;
        }
        long stop = System.currentTimeMillis();
        long zeit = stop - start;
        System.out.println(zeit);
    }

    public void aufg2() {  //n^2
        sum = 0;
        long start = System.currentTimeMillis();
        for (int i=0; i<n; i++)
            for (int j=0; j<n; j++)
                sum++;
        long stop = System.currentTimeMillis();
        long zeit = stop - start;
        System.out.println(zeit);
    }

    public void aufg3() {  //n(n-1)/2
        sum = 0;
        int j = 0;
        long start = System.currentTimeMillis();
        for (int i=0; i<n; i++)
            for (j=0; j<i; j++)
                sum++;
        long stop = System.currentTimeMillis();
        long zeit = stop - start;
        System.out.println(zeit);
    }

    public static void main(String[] args)
    {
        complexity comp = new complexity();
        comp.aufg1();
        comp.aufg2();
        comp.aufg3();
    }
}


