public class Fibonacci2 {

    static int[] fib(int n) {
        int[] werte = new int[n];
        werte[0] = werte[1] = 1;
        for (int i=2; i<n; i++) {
            werte[i] = werte[i - 1] + werte[i - 2];
        }

        return werte;
    }

    public static void main(String[] args) {
        int n = 20;
        int[] werte = fib(n);
        for (int i=0; i<n; i++) {
            System.out.println("hof(" + (i+1) + "): " + werte[i]);
        }
    }
}
