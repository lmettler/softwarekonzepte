// Das ist die Klasse Adresse.
//
public class Adresse
{
    public String name;
    public String vorname;
    public String strasseUndNummer;
    public int plz;
    public String ort;

    public Adresse(String n, String v, String s, int p, String o) {
        name = n; vorname = v;
        strasseUndNummer = s;
        plz = p; ort = o;
    }


    void println() {
        System.out.println(vorname + " " + name);
        System.out.println(strasseUndNummer);
        System.out.println(plz + " " + ort);
    }
}