public class ComplexNumber {
	private double real;
	private double imaginary;

	public ComplexNumber() {

	}

	public ComplexNumber(double real, double imaginary) {
		this.real = real;
		this.imaginary = imaginary;
	}

	public double getReal() {
		return real;
	}

	public double getImaginary() {
		return imaginary;
	}

	public ComplexNumber add(ComplexNumber number) {
		ComplexNumber result = new ComplexNumber(); //neues Objekt Typ ComplexNumber erstellen
		result.real = this.real + number.real; //double real vom Objekt c2 wird zum Objekt c1 hinzugefügt und im Objekt result gespeichert
		result.imaginary = this.imaginary + number.imaginary; // gleich wie oben einfach mit double imaginary
		return result;
	}

	public double abs() {
		return java.lang.Math.sqrt(real*real + imaginary*imaginary); // Betrag beider Zahlen
	}

	public String toString() {
		StringBuffer buf = new StringBuffer();
		buf.append(real);
		if (imaginary >= 0) {
			buf.append("+");
		}
		buf.append(imaginary);
		buf.append("i");
		return buf.toString();
	}
}
